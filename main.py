import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="Rootwurd4343!",
    database="pantry"
)

cursor = db.cursor()

#cursor.execute('CREATE table in pantry ')

class Ingredient:

    def __init__(self, name, quantity=0, measurement=''):
        self.name = name
        self.quantity = quantity
        self.measurement = measurement


class Recipe:
    next_id = 0

    def __init__(self, name, time, cuisine, ingredients=[], rec_method=[]):
        self.id = Recipe.next_id
        Recipe.next_id += 1
        self.name = name
        self.time = time
        self.cuisine = cuisine
        self.ingredients = ingredients
        self.rec_method = rec_method

    def print_recipe(self):
        print(self.name)
        print('='*20)
        print(str(self.time) + ' Mins')
        print(self.cuisine)
        print()
        print('Ingredients')
        print('---------------')
        for i in self.ingredients:
            print(str(i.quantity) + i.measurement + ' ' + i.name)
        print()
        print('Method')
        print('---------------')
        for j in self.rec_method:
            count = 1
            print(str(count) + '. ' + j)
            count += 1
        print()


class Pantry:

    def __init__(self, content=[]):
        self.contents = content

    def get_contents(self):
        return self.contents

    def print_pantry(self):
        print('This Pantry contains: ')
        for i in self.contents:
            print(str(i.quantity) + i.measurement + ' ' + i.name)

    def add_ingredient(self, to_add):
        self.contents.append(to_add)

    def remove_ingredient(self, to_remove):
        self.contents.remove(to_remove)


oil = Ingredient('oil', 250, 'ml')
chicken = Ingredient('chicken', 500, 'g')
paprika = Ingredient('paprika', 0.5, 'g')
oyster_sauce = Ingredient('oyster sauce', 2, 'ml')

p1_ingredients = [oil, chicken, paprika, oyster_sauce]
p1 = Pantry(p1_ingredients)

r1ingredients = [oil, chicken, paprika, oyster_sauce] #maybe just pass name and quantity?
r1method = ['Add oil to pan, let heat up', 'Mix paprika and chicken, then add to pan',
            'Fry for 5 -10 minutes or until cooked', 'Add oyster sauce and coat chicken']

r1 = Recipe('Weird ass chicken', 15, 'Asian', r1ingredients, r1method)

recipes = [r1]


def new_ingredient():
    print("Enter ingredient name")
    ing_name = input()
    print('Enter Ingredient quantity')
    ing_quantity = input()
    print('Enter Ingredient measurement type. e.g. ml, g ..')
    ing_measurement = input()
    n_ingredient = Ingredient(ing_name, ing_quantity, ing_measurement)
    #p1.add_ingredient(newIngredient)
    return n_ingredient


def get_method():
    print("Enter each step in the method one by one")
    new_method = []
    while True:
        print('Enter the next step. Q to stop')
        line = input()
        if line == 'Q':
            print("Method Saved!")
            break
        else:
            new_method.append(line)
            continue
    return new_method


while True:
    print("What do you want to do?")
    action = input("Add [I]ngredient, Add [R]ecipe, [P]rint or [Q]uit?")
    if action not in 'IRPQ' or len(action) != 1:
        print("U Wot m8?!?")
        continue
    if action == 'I':
        print("How many ingredients would you like to add?")
        times = input()
        for x in range(int(times)):
            temp = new_ingredient()
            p1.add_ingredient(temp)
        continue
    elif action == 'R':
        print('Enter Recipe name')
        rName = input()
        print('Enter Recipe time (in mins)')
        rTime = input()
        print('Enter Recipe Cuisine')
        rCuisine = input()
        print("How many ingredients would you like to add?")
        times = input()
        rIngredients = []
        for x in range(int(times)):
            rIngredients.append(new_ingredient())
        rMethod = get_method()
        newRecipe = Recipe(rName, rTime, rCuisine, rIngredients, rMethod)
        recipes.append(newRecipe)
    elif action == 'P':
        while True:
            print('Would you like to print your [P]antry or your [R]ecipes?')
            choice = input()
            if choice not in 'PR' or len(choice) != 1:
                print("U Wot m8?!?")
                continue
            elif choice == 'P':
                p1.print_pantry()
                break
            elif choice == 'R':
                for x in recipes:
                    x.print_recipe()
                break
    elif action == 'Q':
        print('Goodbye!')
        break
